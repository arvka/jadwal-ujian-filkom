﻿
# JADWAL UJIAN FILKOM

This is an application for showing exam schedule in Computer Science Faculty in Brawijaya University.

## GETTING STARTED

Download it as zip or clone this, then upload it in phonegap or you can using cordova for turning this source code into apk file.

## Built With

* [Visual Studio Code](https://code.visualstudio.com/) - Text editor
* [Apache Cordova](https://cordova.apache.org/) - For building application
* [Filkom exam API link](filkom.ub.ac.id/info/jadwal_ujian)
* Javascript using jQuery
* Brain - Used for thinking
* Mouth - Used for swearing in javanese if errors occurr
* Hand - For typing and do useless thing for relaxation

## Authors

* **Arka Fadila Yasa**
* **Fikar Mukamal**
* **Adi Setyo Nugroho**
* **Ofi Noviyanti**
* **Rizaldy Firmansyah**
* **Sindy Alvionita**

## License

See in license page and of course each member of this team have driving license.

## Thanks to 

 - Allah, our almighty god. So we can complete this project.
 - Our parents.
 - Specific platform programming course lecture.
 - My lov... eh wait, i'm still single.
 - Random person who read this readme file, congratulations !! yout are the one of the rarest person in the Round Earth.

