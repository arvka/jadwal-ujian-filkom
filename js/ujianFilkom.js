var hariPilih = "null", tanggalPilih = "null", prodiPilih = "null";


var json_jadwal_ujian; //variabel global berisi objek json jadwal ujian

$(window).on("load", function () {

    //Fungsi untuk mengambil json
    var proxy = 'https://cors-anywhere.herokuapp.com/';
    $.ajax({
        type: "GET",
        dataType: "json",
        url: proxy + "filkom.ub.ac.id/info/jadwal_ujian",
        // url: "https://api.myjson.com/bins/k8zsz", // <-- SELF HOSTED JSON, AGAR BISA SELALU DIAKSES
        crossDomain: true,
        success: function (result) {
            json_jadwal_ujian = result;
            jadwal_hari_ini();
            getOpsiHari();
            getAllJadwal();
        },
        error: function () {
            alert("Terjadi kendala dalam koneksi ke server.")
        },
    });

    /******************** END ********************/

    // Fungsi untuk mengambil opsi hari untuk dropdown pada filter hari
    var getOpsiHari = function () {
        var setter = "";
        $.each(json_jadwal_ujian, function (hari) {
            setter += "<option value='" + hari + "'>" + hari + "</option>";
        });
        $("#selectHari").append(setter);
    }

    //fungsi yang dijalankan jika pilihan dari dropdown hari berubah 
    $('#selectHari').change(function () {
        var filterHari = $('#selectHari').find(":selected").val();
        $("#selectedHari").html(filterHari);
        getOpsiTanggal(filterHari);
    });

    // Fungsi untuk mengambil opsi tanggal untuk dropdown pada filter tanggal
    var getOpsiTanggal = function (hariSelect) {
        var setter = "";
        var jumlahMinggu = 2;
        var filterHari = $('#selectHari').find(":selected").text();
        switch (hariSelect) {
            case "senin":
                setter += "<option value='null' selected>Tanggal Ujian</option><option value='2017-12-18'>2017-12-18</option>";
                break;
            case "selasa":
                setter += "<option value='null' selected>Tanggal Ujian</option><option value='2017-12-19'>2017-12-19</option>";
                break;
            case "rabu":
                setter += "<option value='null' selected>Tanggal Ujian</option><option value='2017-12-20'>2017-12-20</option>";
                setter += "<option value='2017-12-27'>2017-12-27</option>";
                break;
            case "kamis":
                setter += "<option value='null' selected>Tanggal Ujian</option><option value='2017-12-21'>2017-12-21</option>";;
                setter += "<option value='2017-12-28'>2017-12-28</option>";
                break;
            case "jumat":
                setter += "<option value='null' selected>Tanggal Ujian</option><option value='2017-12-22'>2017-12-22</option>";
                setter += "<option value='2017-12-29'>2017-12-29</option>";
                break;
            case "sabtu":
                setter += "<option value='null' selected>Tanggal Ujian</option><option value='2017-12-23'>2017-12-23</option>";
                break;
        }
        $("#selectTanggal").html(setter);
    }

    //fungsi yang dijalankan jika pilihan pada dropdown selectProdi berubah
    $('#selectProdi').change(function () {
        var filterProdi = $('#selectProdi').find(":selected").val();
        $("#selectedProdi").html(filterProdi);
    });

    //fungsi yang dijalankan ketika pilihan pada semua dropdown berubah untuk filtering data jadwal
    $("select.filteria").change(function(){
        var filters = $.map($("select.filteria").toArray(), function(e){
            return $(e).val();
        }).join(".");
        console.log(filters);
        $("ul#all-jadwal").find("li").hide();
        $("ul#all-jadwal").find("li." + filters).show();
    });
});

//fungsi untuk mendapatakan semua data jadwal yang ada pada JSON
var getAllJadwal = function () {
    var jadwal = "";
    $.each(json_jadwal_ujian, function (hari) {
        $.each(this, function (ruang) {
            $.each(this, function () {
                var img = "<img src='img/icon-doc.png' style='width : 100%; height : 100%; margin: 1%;'>";
                var h3 = "<h3>" + hari.toUpperCase() + ", " + format_tanggal(this.tgl,1) + "</h3>";
                var h2 = "<h2>" + this.matakuliah + "</h2>";
                var p1 = "<p>" + "Dosen : " + this.dosen + "</p>";
                var p2 = "<p>" + prodi(this.prodi) + " - " + this.kelas + "</p>";
                var p3 = "<p>Ruang : " + this.ruang + "</p>";
                var p4 = "<p>Waktu : " + this.jam_mulai + " - "+ this.jam_selesai + "</p>";
                var li = "<li class='null "+hari+" "+this.tgl+" "+this.prodi+" "+this.jenis+"'>" +h3+ h2 + p1 + p2 + p3 + p4 + "</li>";
                jadwal += li;
                adaJadwal = true;
            });
        });
    });
    $("#all-jadwal").append(jadwal);
}

//dua fungsi dibawah untuk menampilkan loading ketika jadwal dimuat pada halaman awal
$(document).on('pagebeforecreate', '#daftar-jadwal', function () {
    setTimeout(function () {
        $.mobile.loading('show');
    }, 1);
});

$(document).on('pageshow', '#daftar-jadwal', function () {
    setTimeout(function () {
        $.mobile.loading('hide');
    }, 3000);
});

//Menampilkan semua jadwal yang ada pada hari ini
var jadwal_hari_ini = function () {
    var adaJadwal = false;
    var jadwal = "";
    var date = new Date();
    var tanggal = date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate();
    var tanggal_sekarang = format_tanggal(tanggal, 0);
    var hariSekarang = ["Senin","Selasa","Rabu","Kamis","Jumat","Sabtu","Minggu"];

    $.each(json_jadwal_ujian, function (hari) {
        $.each(this, function (ruang) {
            $.each(this, function () {
                if (this.tgl == tanggal_sekarang) {
                    hariSekarang = hari;
                    var img = "<img src='img/icon-doc.png' style='width : 100%; height : 100%; margin: 1%;'>";
                    var h2 = "<h2>" + this.matakuliah + "</h2>";
                    var p1 = "<p>" + prodi(this.prodi) + " - " + this.kelas + "</p>";
                    var p2 = "<p>Ruang : " + this.ruang + "</p>";
                    var p3 = "<p>Waktu : " + this.jam_mulai + " - "+ this.jam_selesai + "</p>";
                    var li = "<li>" + img + h2 + p1 + p2 + p3 + "</li>";
                    jadwal += li;
                    adaJadwal = true;
                }
            });
        });
    });

    $("#divider-tanggal").html(hariSekarang[date.getDay()-1].toUpperCase() +", "+format_tanggal(tanggal, 1));
    $("#hari-ini").html(hariSekarang[date.getDay()-1].toUpperCase() +", "+format_tanggal(tanggal, 1));

    if (!adaJadwal) {
        $("#jadwal-ditemukan").addClass("hide");
        $("#jadwal-tidak-ditemukan").removeClass("hide");
    } else {
        $("#jadwal-hari-ini").append(jadwal);
        $("#jadwal-hari-ini").listview("refresh");
    }
};

//Fungsi untuk memformat tanggal
var format_tanggal = function (str, model) {
    console.log(str);

    
    dateData = str.split("-");
    var date = new Date(dateData[0],dateData[1],dateData[2]);

    var tahun = dateData[0];
    var bulan = parseInt(dateData[1]) + 1;
    var hari = dateData[2];
    var harike = date.getDay();
    
    console.log(harike);

    bulan = bulan > 12 ? 12 : bulan;
    harike = harike - 1 < 0 ? 7 : harike;
    
    var nama_bulan = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

    if (hari < 10) {
        hari = "0" + hari;
    }

    if (bulan < 10) {
        bulan = "0" + bulan;
    }

    var hari_ini;
    switch (model) {
        case 0: hari_ini = tahun + "-" + bulan + "-" + hari; break;
        case 1: hari_ini = hari + " " + nama_bulan[parseInt(bulan)].toUpperCase() + " " + tahun;
    }

    return hari_ini;
};

//Fungsi untuk mendapatkan nama jurusan berdasarkan kodeProdi
var prodi = function (kodeProdi) {
    switch (kodeProdi) {
        case "SI":
            return "Sistem Informasi";
            break;
        case "SISKOM":
            return "Teknik Komputer";
            break;
        case "ILKOM":
            return "Teknik Informatika";
            break;
        case "2015010001":
            return "Pendidikan Teknologi Informasi";
            break;
        case "2016040001":
            return "Teknologi Informasi";
            break;
        case "2014120001":
            return "Magister Ilmu Komputer/Informatika";
            break;
    }
};